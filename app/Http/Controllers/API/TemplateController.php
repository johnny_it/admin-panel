<?php

namespace App\Http\Controllers\API;

use App\Field;
use App\Http\Resources\TemplateFieldsListResource;
use App\Http\Resources\TemplateListResource;
use App\Template;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TemplateController extends Controller
{
    public function index()
    {
        $templates = Template::all();
        return TemplateListResource::collection($templates);
    }

    public function list()
    {
        $templates = Template::published()->get();
        return TemplateListResource::collection($templates);
    }

    public function tabs($id)
    {
        $template = Template::with(['fields' => function ($query) {
            $query->where('tab', true);
            $query->where('published', true);
            $query->orderBy('order', 'asc');
        }])->findOrFail($id);
        return TemplateFieldsListResource::collection($template->fields);
    }

    public function fields($id)
    {
        $template = Template::with(['fields' => function ($query) {
            $query->where('tab', false);
            $query->where('published', true);
            $query->orderBy('order', 'asc');
        }])->findOrFail($id);
        return TemplateFieldsListResource::collection($template->fields);
    }

}
