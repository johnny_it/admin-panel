<?php

namespace App\Http\Controllers\API;

use App\Gallery;
use App\Http\Resources\PostGalleryResource;
use App\Traits\ManageFilesTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    use ManageFilesTrait;

    public function upload(Request $request)
    {
        return $this->uploadImage($request, 'gallery', true);
    }

}
