<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\TreeResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index() {
        $items = Category::orderBy('order', 'ASC')->get();
        $tree = buildTree($items);
        return TreeResource::collection($tree);
    }
    public function list()
    {
        $items = Category::published()->orderBy('order', 'ASC')->get();
        $tree = buildTree($items);
        return TreeResource::collection($tree);
    }

    public function order(Request $request)
    {
        if(!$request->has('order')) return;
        foreach($request->order as $id=>$item) {
            Category::where('id', $id)->update(['parent_id'=>$item['parent'], 'order'=> $item['order']]);
        }
    }

    public function show($id)
    {
        $item = Category::findOrFail($id);
        return new CategoryResource($item);
    }

    public function destroy($id)
    {
        $item = Category::findOrFail($id);
        $item->delete();
    }

    public function store(Request $request)
    {
        $item = $request->isMethod('put') ? Category::findOrFail($request->id) : new Category;
        $item->title = $request->title;
        $item->description = $request->description;
        $item->published = $request->has('published') ? $request->published : true;

        if(!$request->isMethod('put') || $item->parent_id != $request->parent_id) {
            $item->parent_id = $request->parent_id;
            $item->order = Category::where('parent_id', $request->parenet_id)->max('order') + 1;
        }

        $item->save();

        return $item;
    }
}
