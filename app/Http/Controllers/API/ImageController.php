<?php

namespace App\Http\Controllers\API;

use App\Traits\ManageFilesTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    use ManageFilesTrait;

    public function upload(Request $request)
    {
        return $this->uploadImage($request, 'tmp', true);
    }
    public function uploadPipe(Request $request)
    {
        return $this->uploadImage($request, 'upload', null, true);
    }
    public function multiUpload(Request $request)
    {
        return $this->uploadImages($request, 'upload');
    }
}
