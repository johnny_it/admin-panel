<?php

namespace App\Http\Controllers\API;

//use App\Http\Resources\UserResource;
//use App\Mail\RegistrationComplete;
use App\Http\Resources\UserInfoResource;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
/*use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;*/
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function getUser()
    {
        return new UserInfoResource(auth()->user());
        //$role = auth()->user()->roles[0]->name;
        $user = auth()->user();
        return new UserInfoResource($user);
        dd($user, $user->roles[0]->name);
        //$user->role = $role;
        //return new UserResource($user);
    }

    public function login() {
        if(Auth::user()) return redirect()->route('admin');
        else return view('auth.login');
    }

    public function auth(Request $request)
    {
        $errors = [];
        if($request->email) {
            $user = User::where('email', $request->email)->where('active', true)->first();
            if(!$user) return response(['errors' => ['email' =>'Пожалуйста, введите правильный e-mail адрес.']], 405);
        }
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, $request->has('remember')))
        {
            $user = Auth::user();
            return response(['success' => ['Вход в админку']], 200);
        }
        else {
            $errors['password'] = ['Неверный пароль.'];
            return response(['errors' => $errors], 405);
        }
    }

    public function logout()
    {
        if(Auth::check()) Auth::logout();
        return redirect()->route('home');
    }

}
