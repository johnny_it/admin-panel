<?php

namespace App\Http\Controllers\API;

use App\Gallery;
use App\Http\Resources\PostListResource;
use App\Http\Resources\PostResource;
use App\Http\Resources\TreeResource;
use App\Post;
use App\Traits\ManageFilesTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    use ManageFilesTrait;


    public function index()
    {
        $items = Post::last()->get();
        return PostListResource::collection($items);
    }

    public function type($type)
    {
        $items = Post::published()->ofType($type)->last()->get();
        return PostListResource::collection($items);
    }

    public function listTypeAll($type)
    {
        if($type == 'page') {
            $items = Post::ofType($type)->orderBy('order', 'ASC')->get();
            $tree = buildTree($items);
            return TreeResource::collection($tree);
        }
        else {
            $items = Post::ofType($type)->with('categories')->last()->get();
            return PostListResource::collection($items);
        }
    }

    public function listType($type)
    {
        if($type == 'page') {
            $items = Post::published()->ofType($type)->orderBy('order', 'ASC')->get();
            $tree = buildTree($items);
            return TreeResource::collection($tree);
        }
        else {
            $items = Post::published()->ofType($type)->with('categories')->orderBy('order', 'ASC')->get();
            return PostListResource::collection($items);
        }
    }

    public function store(Request $request)
    {
        $item = $request->isMethod('put') ? Post::findOrFail($request->id) : new Post;
        $item->type = $request->type;
        $item->title = $request->title;
        $item->url = $request->url;
        $item->description = $request->description;
        $item->content = $request['content'];
        $item->meta_title = $request->meta_title;
        $item->meta_keys = $request->meta_keys;
        $item->meta_desc = $request->meta_desc;
        $item->template_id = $request->template_id;
        $item->published = $request->has('published') ? $request->published : true;


        if(!$request->isMethod('put')) {
            $item->user_id = Auth::user()->id;
        }
        else {
            $item->user_id = $request->user_id ? $request->user_id : Auth::user()->id;
        }

        if(!$request->isMethod('put') || $item->parent_id != $request->parent_id) {
            $item->parent_id = $request->parent_id;
            $item->order = Post::where('parent_id', $request->parenet_id)->max('order') + 1;
        }

        if ($item->save()) {
            $categories = $request->has('categories') ? $request->categories : [];
            $item->categories()->sync($categories);

            if($request->image && $request->image != $item->image) {
                $this->deletePublicFile($item->image);
                $imagePath = 'assets/images/'.$item->type.'/';
                $fullName = $this->movePublicFile($request->image, $imagePath);
                $item->image = $fullName;
                $item->save();
            }
            elseif(!$request->image && $item->image) {
                $this->deletePublicFile($item->image);
                $item->image = Null;
                $item->save();
            }

            $fields = [];
            foreach ($request->fields as $field) {
                if($field['value'] && !is_null($field['value'])) $fields[$field['id']] = ['value'=> $field['value']];
            }
            $item->fields()->sync($fields);

            $this->updatePostGallery($item, $request->gallery);

        }

        return $item;

    }

    public function show($type, $id)
    {
        //$item = Post::where('type', $type)->with(['categories', 'fields', 'links', 'gallery' => function($query) {$query->orderBy('order', 'ASC');}])->findOrFail($id);
        //$item = Post::ofType($type)->findOrFail($id);
        $item = Post::ofType($type)->with([
            'fields',
            'categories',
            'gallery' => function ($query) {
                $query->orderBy('order', 'ASC');
            }
        ])->findOrFail($id);
        return new PostResource($item);
    }

    public function order(Request $request)
    {
        if(!$request->has('order')) return;
        foreach($request->order as $id=>$item) {
            Post::where('id', $id)->update(['parent_id'=>$item['parent'], 'order'=> $item['order']]);
        }
    }

    public function destroy($id)
    {
        $item = Post::with(['children'])->findOrFail($id);
        $this->removeChildrenItems($item->children);
        $this->destroyPost($item);
    }

    private function updatePostGallery(Post $item, $gallery) {
        $itemOldGalleryImages = $item->gallery->pluck('url', 'id')->toArray();
        if(!$gallery) {
            $this->deleteGalleryImages($itemOldGalleryImages);
            return;
        }
        $order = 0;
        foreach ($gallery as $image) {
            if(isset($image['id'])) {
                $img = Gallery::find($image['id']);
                $img->url = $image['url'];
                $img->title = $image['title'];
                $img->alt = $image['title'];
                $img->order = $order;
                if(isset($itemOldGalleryImages[$image['id']])) {
                    unset($itemOldGalleryImages[$image['id']]);
                }
                $img->save();
            }
            else {
                $imagePath = 'assets/galleries/' . $item->type.'/'. $item->id .'/';
                $fullName = $this->movePublicFile($image['url'], $imagePath);
                $img = Gallery::create([
                    'url' => $fullName,
                    'title'=>$image['title'],
                    'alt'=> $image['title'],
                    'order' => $order
                ]);
                $item->gallery()->save($img);
            }
            $order++;
        }

        $this->deleteGalleryImages($itemOldGalleryImages);
    }

    private function deleteGalleryImages($galleryImages) {
        if(count($galleryImages)) {
            $galleryImagesIds = [];
            foreach ($galleryImages as $id=>$url) {
                $this->deletePublicFile($url);
                array_push($galleryImagesIds, $id);
            }
            Gallery::whereIn('id', $galleryImagesIds)->delete();
        }
    }

    private function destroyPost(Post $item) {
        $image = $item->image;
        if ($item->delete()) {
            if($image) $this->deletePublicFile($image);
            $this->deletePublicDir('assets/galleries/' . $item->type.'/'. $item->id);
            $item->gallery()->delete();
            return $item;
        }
    }

    private function removeChildrenItems($children) {
        if(!count($children)) return;
        foreach ($children as $item) {
            if(count($item->children)) $this->removeChildrenItems($item->children);
            $this->destroyPost($item);
        }
    }
}
