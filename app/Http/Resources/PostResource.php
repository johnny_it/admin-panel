<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'template_id' => $this->template_id,
            'parent_id' => $this->parent_id,
            'type' => $this->type,
            'title' => $this->title,
            'url' => $this->url,
            'image' => $this->image,
            'description' => $this->description,
            'content' => $this->content,
            'meta_title' => $this->meta_title,
            'meta_keys' => $this->meta_keys,
            'meta_desc' => $this->meta_desc,
            'published' => $this->published,
            'published_at' => $this->published_at,
            'order' => $this->order,

            'fields' => PostFieldsResource::collection($this->whenLoaded('fields')),
            'gallery' => PostGalleryResource::collection($this->whenLoaded('gallery')),
            'categories' => PostCategoriesResource::collection($this->whenLoaded('categories')),
            /*'categories' => ListIdResource::collection($this->whenLoaded('categories')),
            'menu' => $this->whenLoaded('links', function () {
                $links = $this->links;
                if (count($links)) {
                    return $links[0]->menu_id;
                } else {
                    return null;
                }
            }),
            'fields' => PostFieldsResource::collection($this->whenLoaded('fields')),
            'gallery' => PostGalleryResource::collection($this->whenLoaded('gallery')),
            'catlevels' => CategoryLevelResource::collection($this->whenLoaded('categories')),*/
        ];
    }
}
