<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PostListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'title' => $this->title,
          'url' => $this->url,
          'created' => Carbon::parse($this->created_at)->toDateTimeString(),
          'updated' => Carbon::parse($this->updated_at)->toDateTimeString(),
          'published' => $this->published,
          'categories' => PostCategoriesListResource::collection($this->whenLoaded('categories')),
        ];
    }
}
