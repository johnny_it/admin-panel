<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    use Sluggable;

    protected $guarded = [];

    public function sluggable()
    {
        return [
          'name' => [
            'source' => 'title'
          ]
        ];
    }

    public function scopePublished($query)
    {
        $query->where('published', true);
    }


    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function fields()
    {
        return $this->belongsToMany('App\Field', 'field_template')->withPivot('order')->as('fields');
    }
}
