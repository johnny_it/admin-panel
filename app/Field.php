<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $guarded = [];

    public function scopePublished($query)
    {
        $query->where('published', true);
    }

    public function templates()
    {
        return $this->belongsToMany('App\Template', 'field_template')->withPivot('order')->as('templates');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Post', 'field_post')->withPivot('value')->as('posts');
    }
}
