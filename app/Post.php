<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use Sluggable;

    protected $guarded = [];

    public function sluggable()
    {
        return [
          'url' => [
            'source' => 'title'
          ]
        ];
    }

    public function child()
    {
        return $this->hasMany(self::class, 'parent_id')->orderBy('order', 'ASC');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id')->orderBy('order', 'ASC')->with('children');
    }

    /*public function childrenGallery()
    {
        return $this->hasMany(self::class, 'parent_id')->with('gallery')->with('childrenGallery');
    }*/


    public function scopePublished($query)
    {
        $query->where('published', true);
    }
    public function scopeUnPublished($query)
    {
        $query->where('published', false);
    }

    public function scopeOfType($query, $type)
    {
        $query->where('type', $type);
    }

    public function scopeLast($query)
    {
        $query->orderBy('updated_at', 'desc');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function template()
    {
        return $this->belongsTo('App\Template');
    }

    public function fields()
    {
        return $this->belongsToMany('App\Field', 'field_post')->withPivot('value')->as('fields');
    }


    public function gallery()
    {
        return $this->morphMany('App\Gallery', 'galleryable');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'post_category')->withPivot('order')->as('categories');
    }

    /*public function home()
    {
        return $this->belongsToMany('App\Field', 'field_post')->withPivot('value');
    }*/
}
