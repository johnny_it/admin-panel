<?php

namespace App;

#use Illuminate\Contracts\Auth\MustVerifyEmail;
#use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name', 'email', 'password', 'image', 'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    /*protected $casts = [
        'email_verified_at' => 'datetime',
    ];*/

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function attachRole($role)
    {
        if (is_object($role)) {
            $role = $role->getKey();
        } elseif (is_array($role)) {
            $role = $role['id'];
        } else {
            $role = Role::where('name', '=', $role)->first();
        }

        $this->roles()->attach($role);
    }

    public function detachRole($role)
    {
        if (is_object($role)) {
            $role = $role->getKey();
        } elseif (is_array($role)) {
            $role = $role['id'];
        } else {
            $role = Role::where('name', '=', $role)->first();
        }

        $this->roles()->detach($role);
    }

    public function attachRoles($roles)
    {
        foreach ($roles as $role) {
            $this->attachRole($role);
        }
    }

    public function detachRoles($roles)
    {
        foreach ($roles as $role) {
            $this->detachRole($role);
        }
    }

    public function hasRole($roles, $requireAll = false)
    {
        if (is_string($roles)) {
            return $this->roles->contains('name', $roles);
        }
        else {
            if(is_array($roles)) {
                foreach ($roles as $role) {
                    if($this->roles->contains('name', $role)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }
}
