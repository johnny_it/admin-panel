<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function child()
    {
        return $this->hasMany(self::class, 'parent_id')->orderBy('order', 'ASC');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id')->orderBy('order', 'ASC')->with('children');
    }

    public function scopePublished($query)
    {
        $query->where('published', true);
    }
    public function scopeUnPublished($query)
    {
        $query->where('published', false);
    }

    public function posts()
    {
        return $this->belongsToMany('App\Post', 'post_category')->withPivot('order')->as('posts');
    }
}
