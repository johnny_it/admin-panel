<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Image500x500 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(500, 500, function ($constraint) {
            $constraint->upsize();
        });
    }
}
