<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Image370x450 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(370, 450, function ($constraint) {
            $constraint->upsize();
        });
    }
}
