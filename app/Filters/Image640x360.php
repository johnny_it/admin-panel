<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Image640x360 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(640, 360, function ($constraint) {
            $constraint->upsize();
        });
    }
}
