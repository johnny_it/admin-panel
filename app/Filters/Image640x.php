<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Image640x implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->resize(640, null, function ($constraint) {
            $constraint->aspectRatio();
        });
    }
}
