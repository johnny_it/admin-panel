<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Image480x implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->resize(480, null, function ($constraint) {
            $constraint->aspectRatio();
        });
    }
}
