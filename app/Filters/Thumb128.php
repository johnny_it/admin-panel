<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Thumb128 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        /*return $image->fit(128, 128, function ($constraint) {
            $constraint->upsize();
        });*/
        // resize the image to a width of 300 and constrain aspect ratio (auto height)
        return $image->resize(128, 128, function ($constraint) {
            $constraint->aspectRatio();
        });
        //return $image->resizeCanvas(128, 128);
    }
}
