<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Avatar implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->resize(100, null, function ($constraint) {
            $constraint->aspectRatio();
        });
    }
}
