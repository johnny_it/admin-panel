<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Image540x400 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(540, 400, function ($constraint) {
            $constraint->upsize();
        });
    }
}
