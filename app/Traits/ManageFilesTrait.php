<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

trait ManageFilesTrait {

    private $image_ext = ['jpg', 'jpeg', 'png', 'gif'];
    private $document_ext = ['doc', 'docx', 'pdf', 'odt'];

    public function uploadImage(Request $request, $folder = null, $tmp = null, $dtSort = null, $size = null)
    {
        $max_size = (int)ini_get('upload_max_filesize') * 1000;
        $img_ext = implode(',', $this->image_ext);

        $request->validate([
            'file' => 'required|file|mimes:' . $img_ext . '|max:' . $max_size
        ]);

        $prePath = $tmp ? 'tmp/' : 'assets/';
        $folder = $folder ? $prePath.$folder.'/' : 'assets/';

        $file = $request->file('file');
        $image = $this->imageStore($file, $folder, $dtSort);
        if($image) return $image;

    }

    /*public function uploadImages(Request $request, $folder = null, $tmp = false)
    {
        $max_size = (int)ini_get('upload_max_filesize') * 1000;
        $img_ext = implode(',', $this->image_ext);

        if(!$request->hasFile('file')) return;

        $rules = [
          'file' => 'required|file|mimes:' . $img_ext . '|max:' . $max_size
        ];
        $customMessages = [
          'mimes' => 'Файл должен быть следующего типа: ' . $img_ext
        ];

        $prePath = $tmp ? 'tmp/' : 'assets/';
        $folder = $folder ? $prePath.$folder.'/' : 'assets/';

        $images = [];
        $files = $request->file('file');
        $errors = [];

        foreach ($files as $file) {
            $validator = Validator::make($file, $rules, $customMessages);

            if ($validator->fails()) {
                array_push($errors, $validator->errors()->all());
                continue;
            }

            $image = $this->imageStore($file, $folder, $dtSort);
            if($image) array_push($images, $image);
        }
        return $images;
    }*/

    public function deletePublicFile($file): string
    {
        $fullName = public_path($file);
        if (file_exists($fullName)) {
            File::delete($fullName);
        }
        return $fullName;
    }

    public function movePublicFile($file, $imagePath, $clone = false): string
    {
        if(!is_dir($imagePath)){
            mkdir($imagePath, 0755, true);
        }
        $actual_name = Str::slug(File::name($file), '-');
        $original_name = $actual_name;
        $extension = File::extension($file);

        $name = $actual_name . "." . $extension;

        $i = 1;
        while (file_exists($imagePath . $actual_name . "." . $extension)) {
            $actual_name = $original_name . '-' . $i;
            $name = $actual_name . "." . $extension;
            $i++;
        }
        $fullName = $imagePath.$name;

        if($clone) { File::copy($file, $fullName); }
        else {
            File::move($file, $fullName);
        }
        return $fullName;
    }

    public function deletePublicDir($dir): string
    {
        $fullDir = public_path($dir);
        File::deleteDirectory($fullDir);
        return $fullDir;
    }

    private function imageStore($file, $folder, $dtSort = null, $size = null)
    {
        $filename = $file->getClientOriginalName();
        $filename = Str::slug(pathinfo($filename, PATHINFO_FILENAME));
        $ext = $file->getClientOriginalExtension();
        $type = $this->getType(strtolower($ext));
        $dtFolder = $dtSort ? now()->year.'/'.now()->month.'/' : '';
        $path = $folder . $type . '/'.$dtFolder;
        $newFileName = $filename . '.' . $ext;

        if (!is_dir($path)) {
            //Directory does not exist, so lets create it.
            mkdir($path, 0755, true);
        }
        if ($size) {
            $w = trim($size[0]);
            $h = trim($size[1]);
            if ($w == 'auto' || $h == 'auto') {
                $ratio = function ($constraint) {
                    $constraint->aspectRatio();
                };
            } else {
                $ratio = null;
            }

            $image = Image::make($file)->resize($w, $h, $ratio)->save($path . $newFileName);
        } else {
            $image = $file->move(public_path($path), $newFileName);
        }
        if ($image) {
            return $path . $newFileName;
        }
        else return false;
    }

    private function getType($ext)
    {
        if (in_array($ext, $this->image_ext)) {
            return 'images';
        }

        if (in_array($ext, $this->document_ext)) {
            return 'docs';
        }
    }

}
