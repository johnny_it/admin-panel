<?php
Route::get('', 'PostController@home')->name('home');
Route::get('case', 'PostController@case')->name('case');

Route::get('login', 'API\AuthController@Login')->name('login'); //editor
Route::post('api/login', 'API\AuthController@auth')->name('api.login'); //editor
Route::get('api/logout', 'API\AuthController@logout')->name('api.logout'); //editor

Route::group(['middleware' => 'auth', 'prefix' => 'api', 'namespace' => 'API' ], function () {
    Route::get('get-user', 'AuthController@getUser');

    //Route::post('posts/clone', 'PostController@clone');
    Route::post('posts/order', 'PostController@order');
    Route::get('posts/{type}', 'PostController@type');
    Route::get('posts/{type}/list', 'PostController@listType');
    Route::get('posts/{type}/listall', 'PostController@listTypeAll');
    //Route::post('posts/{type}/order', 'PostController@order');
    Route::get('posts/{type}/{id}', 'PostController@show');
    Route::resource('posts', 'PostController', [
      'except' => ['index', 'create', 'edit', 'update', 'show']
    ]);
    Route::put('posts', 'PostController@store');


    Route::post('categories/order', 'CategoryController@order');
    Route::get('categories/list', 'CategoryController@list');
    Route::resource('categories', 'CategoryController', [
      'except' => ['create', 'edit', 'update']
    ]);
    Route::put('categories', 'CategoryController@store');


    Route::post('upload/image', 'ImageController@upload');
    Route::post('upload/imagepipe', 'ImageController@uploadPipe');
    //Route::post('upload/images', 'ImageController@multiUpload');
    Route::post('gallery/upload', 'GalleryController@upload');

    Route::get('templates', 'TemplateController@index');
    Route::get('templates/list', 'TemplateController@list');
    Route::get('templates/{template}/tabs', 'TemplateController@tabs');
    Route::get('templates/{template}/fields', 'TemplateController@fields');

    Route::get('test', 'TestController@test');

    Route::view('', 'admin.app')->where('any', '.*')->name('admin');
    Route::view('{any}', 'admin.app')->where('any', '.*');
});

Route::group(['middleware' => 'auth', 'prefix' => 'admin', 'namespace' => 'Admin' ], function () {
    Route::view('', 'admin.app')->where('any', '.*')->name('admin');
    Route::view('{any}', 'admin.app')->where('any', '.*');
});
