try {
  window.$ = window.jQuery = require('jquery');
  window.Popper = require('popper.js').default;
  require('bootstrap');
  window.fancybox = require('@fancyapps/fancybox');
} catch (e) {}

$(function () {
  themeInit();
  themeSwitcher();

  console.log('good');
});

callbackFlow();

function themeInit() {
  var root = document.documentElement;
  var theme = window.localStorage.getItem('theme') ? window.localStorage.getItem('theme') : 'dark';
  if(theme == 'dark') {
    root.style.setProperty('--theme-background-color', 'black');
    root.style.setProperty('--theme-color', 'white');
    root.style.setProperty('--theme-color-invert', 'black');
  }
  else {
    root.style.setProperty('--theme-background-color', 'white');
    root.style.setProperty('--theme-color', 'black');
    root.style.setProperty('--theme-color-invert', 'white');
  }
  window.localStorage.setItem('theme', theme);
  $('.theme-link').removeClass('active');
  $('.theme-link[data-theme="'+theme+'"]').addClass('active');
}
function themeSwitcher() {
  if(!$('.theme-link').length) return;
  $('.theme-link').on('click', function (e) {
    e.preventDefault();
    var theme = $(this).data('theme');
    var root = document.documentElement;
    if(theme == 'dark') {
      root.style.setProperty('--theme-background-color', 'black');
      root.style.setProperty('--theme-color', 'white');
      root.style.setProperty('--theme-color-invert', 'black');
    }
    else {
      root.style.setProperty('--theme-background-color', 'white');
      root.style.setProperty('--theme-color', 'black');
      root.style.setProperty('--theme-color-invert', 'white');
    }
    window.localStorage.setItem('theme', theme);
    $('.theme-link').removeClass('active');
    $(this).addClass('active');
  })
}
function callbackFlow() {
  var page = $('body').attr('id');
  var callbackTop = (page == 'page-home') ? 531 : 284;

  $(window).scroll(function () {
    var w = $( window ).width();
    if(w < 1680) return;
    var scrollTop = $(window).scrollTop();
    if (scrollTop > callbackTop) $('.callback').addClass('flow');
    else $('.callback').removeClass('flow');
  });
}
