export function selectTree(nodes, current = null, rootLabel = '-', arr = [{id: null, title: rootLabel}], label='') {
  nodes.forEach((n)=>{
    if(n.id === current) return;

    arr.push({
      id: n.id,
      title: label + n.title,
    })

    if(n.children.length) {
      selectTree(n.children, current, rootLabel, arr, label+'-')
    }
  })
  return arr;
}
