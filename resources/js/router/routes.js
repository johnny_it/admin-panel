import Vue from 'vue'
import VueRouter from 'vue-router';
import Middleware from './middleware';
import MainLayout from './layouts/MainLayout'

Vue.use(VueRouter);

let mid = new Middleware();
let prefix = '/admin';
let routes = [
  {
    //name: 'admin',
    path: prefix,
    component: MainLayout,
    //hidden: true,
    children: [
      {
        name: 'admin',
        path: prefix,
        component: () => import('./views/Dashboard')
      },
      {
        name: 'admin.pages',
        path: prefix+'/pages',
        component: () => import('./views/Pages')
      },
      {
        name: 'admin.page.create',
        path: prefix+'/page',
        component: () => import('./views/Page')
      },
      {
        name: 'admin.page',
        path: prefix+'/pages/:id',
        component: () => import('./views/Page')
      },
      {
        name: 'admin.categories',
        path: prefix+'/categories',
        component: () => import('./views/Categories')
      },
      {
        name: 'admin.posts',
        path: prefix+'/posts',
        component: () => import('./views/Posts')
      },
      {
        name: 'admin.templates',
        path: prefix+'/templates',
        component: () => import('./views/Templates')
      },
      {
        name: 'admin.fields',
        path: prefix+'/fields',
        component: () => import('./views/Fields')
      },
    ]
  },
];
let adminRoutes = [
  /*{
    name: 'admin.test',
    path: prefix+'/test',
    component: require('./components/Test.vue'),
  },*/
];

routes = routes.concat(adminRoutes);

const router = new VueRouter({
  mode: 'history',
  routes
});

router.beforeResolve((to, from, next) => {
  mid.forEach(function (item, i) {
    if(to.name == item.name) {
      if(!item.roles.includes(user.role)) {
        next({
          path: '/admin',
          query: { redirect: to.fullPath }
        });
        return;
      }
    }
  })
  next();
});

export default router;
