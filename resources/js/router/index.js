import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import MainLayout from '../layouts/MainLayout'

/**
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

export const constantRoutes = [
  {
    //name: 'admin',
    path: '',
    component: MainLayout,
    //hidden: true,
    children: [
      {
        name: 'admin',
        path: '',
        component: () => import('../views/Dashboard')
      },
      {
        name: 'admin.pages',
        path: '/pages',
        component: () => import('../views/Pages')
      },
      {
        name: 'admin.page.create',
        path: '/page',
        component: () => import('../views/Page')
      },
      {
        name: 'admin.page',
        path: '/pages/:id',
        component: () => import('../views/Page')
      },
      {
        name: 'admin.categories',
        path: '/categories',
        component: () => import('../views/Categories')
      },
      {
        name: 'admin.category.create',
        path: '/category',
        component: () => import('../views/Category')
      },
      {
        name: 'admin.category',
        path: '/categories/:id',
        component: () => import('../views/Category')
      },
      {
        name: 'admin.posts',
        path: '/posts',
        component: () => import('../views/Posts')
      },
      {
        name: 'admin.post.create',
        path: '/post',
        component: () => import('../views/Post')
      },
      {
        name: 'admin.post',
        path: '/posts/:id',
        component: () => import('../views/Post')
      },
      {
        name: 'admin.templates',
        path: '/templates',
        component: () => import('../views/Templates')
      },
      {
        name: 'admin.fields',
        path: '/fields',
        component: () => import('../views/Fields')
      },
    ]
  },
]

const createRouter = () => new Router({
  mode: 'history', // require service support
  base: '/admin',
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes: constantRoutes
})

const router = createRouter()

/*
router.beforeResolve((to, from, next) => {
  next();
});
*/

export default router
