import router from '../../router';
const state = {
  //token: getToken(),
  user: {
    id: null,
    name: null,
    avatar: null,
    role: null,
  }
}

const getters = {
  user: state => {
    return state.user;
  }
}

const mutations = {
  SET_ID: (state, id) => {
    state.user.id = id
  },
  SET_NAME: (state, name) => {
    state.user.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.user.avatar = avatar
  },
  SET_ROLE: (state, role) => {
    state.user.role = role
  },
}

const actions = {
  // get user info
  getUser({ commit, state }) {
    return new Promise((resolve, reject) => {
      axios.get('/api/get-user')
        .then((res) => {
          const data = res.data
          if (!data) {
            reject('Verification failed, please Login again.')
          }
          //const { role, name, avatar } = data
          const { id, role, name, avatar } = data
          // roles must be a non-empty array
          if (!role) { reject('getInfo: role must be a non-null!')}
          commit('SET_ID', id)
          commit('SET_ROLE', role)
          commit('SET_NAME', name)
          commit('SET_AVATAR', avatar)
          resolve(data)
        })
        .catch((err) => {
          console.log(err);
          reject(error)
        });
    })
  },
}

export default {
  //namespaced: true,
  state,
  getters,
  mutations,
  actions
}
