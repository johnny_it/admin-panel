function getChildrenTree(nodes, parentId) {
  let childTree = {};
  let order = 0;
  nodes.forEach((n)=>{
    childTree[n.id] = {
      parent: parentId,
      order,
    }
    if(n.children.length) Object.assign(childTree, getChildrenTree(n.children, n.id));
    order++;
  })
  return childTree;
}
export default {
  data() {
    return {
      newTree: null,
    }
  },
  methods: {
    newOrder(nodes) {
      let newTree = {};
      if(!nodes.length) return;
      let order = 0;
      nodes.forEach((root)=>{
        newTree[root.data.id] = {
          parent: null,
          order,
        };
        if(root.data.children.length) Object.assign(newTree, getChildrenTree(root.data.children, root.data.id));
        order++;
      })
      this.newTree = newTree;
    }
  }
}
