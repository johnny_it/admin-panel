require('./app-bootstrap');
import Vue from 'vue';

import 'normalize.css/normalize.css' // a modern alternative to CSS resets
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'
import './styles/element-variables.scss'

import App from './App.vue';
import store from './store'
import router from './router'

Vue.use(ElementUI, { locale })

const app = new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
});
