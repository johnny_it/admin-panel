@if(isset($tools) && count($tools))
  @foreach ($tools as $tool)
    <div class="col-lg-4 my-2 tools__item">
      <a href="{{ route('tool', ['url' => $tool['slug']]) }}" class="tools__link">
        <div class="tools__img"><img src="/img/640x360/{{ $tool['image'] }}" alt="{{ $tool['title'] }}"></div>
        <div class="tools__title mt-2">{{ $tool['title'] }}</div>
      </a>
    </div>
  @endforeach
@endif
