<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('partials.head')
<body class="home" id="page-home">
  @include('partials.top-scripts')
  <div class="wrapper">
    @include('partials.header-home')
    @include('partials.nav-main')

    @yield('content')

    @include('partials.footer')
  </div>

  @include('partials.modal')
  <script src="{{ mix('/template/js/script.js') }}"></script>
  @include('partials.bottom-scripts')
  @yield('bottom_scripts')
</body>
</html>
