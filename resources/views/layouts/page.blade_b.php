<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('partials.head')
<body>
@include('partials.top-scripts')
@include('partials.header')

@yield('breadcrumbs')
@yield('content')

@include('partials.follow')
@include('partials.footer')

@if(!Auth::check() || !Auth::user()->active)
  @include('partials.auth-slide-right')
@endif

<script src="{{ mix('/js/script.js') }}"></script>
@include('partials.bottom-scripts')
@yield('bottom_scripts')
</body>
</html>
