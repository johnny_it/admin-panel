<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  @include('partials.head-top-scripts')
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name') }}</title>
  {{--<link rel="stylesheet" href="{{ mix('/template/css/app.css') }}">--}}
</head>
<body>
  @include('partials.top-scripts')
<div id="app">
  <app></app>
</div>

<script>
  window.user = { login : false};
  window.csrf = '{{ csrf_token() }}';
  @auth
    window.user = {
    login: +'{!! Auth::user()->id !!}' ? true : false,
    id: +'{!! Auth::user()->id !!}',
    name: '{!! Auth::user()->name !!}',
    role: '{!! Auth::user()->roles[0]->name !!}',
  }
  @endauth
</script>

<script src="{{ mix('/template/js/app.js') }}"></script>
@include('partials.bottom-scripts')
</body>
</html>
