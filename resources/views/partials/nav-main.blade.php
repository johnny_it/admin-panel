<nav class="navbar menu-main navbar-expand-md">
  <div class="container">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-header" aria-controls="menu-header" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon">
        <svg viewBox='0 0 30 30' xmlns='http://www.w3.org/2000/svg'><path stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 7h22M4 15h22M4 23h22'/></svg>
      </span>
    </button>
    <div class="collapse navbar-collapse mt-4 mt-md-0" id="menu-header">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="/">Главная</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="/login">Вход</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/admin">Админка</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
