<header class="header header-home">
  <div class="container">
    <div class="row">
      <div class="logo col-sm-6 col-md-5 col-lg-4 col-xl-3">

      </div>
      <div class="col-sm-6 col-xl-4">
        <div class="contacts">

        </div>
      </div>
    </div>
    <div class="theme-switcher my-4 mt-lg-0 mx-auto">
      <div class="theme-link theme-dark" data-theme="dark" title="Темная тема"></div>
      <div class="theme-link theme-light" data-theme="light" title="Светлая тема"></div>
    </div>
  </div>
</header>

