<a class="logo__link" href="/" title="{{ config('app.name') }}">
  <picture>
    <source srcset="/template/img/logo.webp" type="image/webp">
    <img src="/template/img/logo.png" alt="{{ config('app.name') }}">
  </picture>
</a>
