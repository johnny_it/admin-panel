<nav class="navbar navbar-main navbar-dark navbar-expand-md">
  <div class="container navbar-main-left">
    <a href="/" class="navbar-brand">
      <picture>
        <source srcset="/assets/images/logo.webp" type="image/webp">
        <img src="/assets/images/logo.png" alt="Pixiko"></picture>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-header"
            aria-controls="menu-header" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  </div>
  <div class="container navbar-main-right">
    <div class="collapse navbar-collapse" id="menu-header">
      {{--@include('partials.nav-main-menu')--}}
    </div>
  </div>
</nav>
