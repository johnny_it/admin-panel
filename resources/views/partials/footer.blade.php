<footer class="footer pt-5 mt-5">
  <div class="container">
    <div class="footer-menu mt-5">
      <div class="row">
        <ul class="nav nav-footer-1 mt-4">
          <li class="nav-item">
            <a class="nav-link" href="/">Главная</a>
          </li>
        </ul>
        <ul class="nav nav-footer-2 mt-4">
          <li class="nav-item">
            <a class="nav-link" href="/login">Вход</a>
          </li>
        </ul>
        <ul class="nav nav-footer-3 mt-4">
          <li class="nav-item">
            <a class="nav-link" href="/admin">Админка</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="footer-copyright py-4">{{ config('app.name') }} ©2019</div>
  </div>
</footer>
