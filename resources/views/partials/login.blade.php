<form class="ajaxauth mt-4" data-url="/api/login" method="POST" action="{{ route('login') }}">
  @csrf

  <div class="form-group row needs-validation" novalidate>
    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
    <div class="col-md-6">
      <input type="email" class="form-control" name="email" required autofocus>
    </div>
  </div>

  <div class="form-group row">
    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

    <div class="col-md-6">
      <input type="password" class="form-control" name="password" required>
    </div>
  </div>

  {{--<div class="form-group row">
    <div class="col-md-6 offset-md-4">
      <div class="form-check">
        <input class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
        <label class="form-check-label" for="remember">{{ __('Remember Me') }}</label>
      </div>
    </div>
  </div>--}}

  <div class="form-group row mb-0">
    <div class="col-md-5 offset-md-4">
      <button type="submit" class="btn btn-pink">{{ __('Login') }}</button>
      <a class="btn btn-link mt-2" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
      {{--@if (Route::has('password.request'))
        <a class="btn btn-link" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
      @endif--}}
    </div>
  </div>
</form>
