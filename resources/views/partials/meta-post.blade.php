@if(isset($post->meta_title))
  <title>{{ $post->meta_title }}</title>
  <meta property="og:title" content="{{ $post->meta_title }}" />
@endif
@if(isset($post->meta_keys))
  <meta name="keywords" content="{{ $post->meta_keys }}">
@endif
@if(isset($post->meta_desc))
  <meta name="description" content="{{ $post->meta_desc }}">
  <meta property="og:description"   content="{{ $post->meta_desc }}" />
  <meta property="twitter:description"   content="{{ $post->meta_desc }}" />
@endif

@switch($post->type)
  @case('page')
    <meta property="og:url" content="{{route('page', ['url'=>$post->slug])}}" />
  @break
  @case('case')
    <meta property="og:url" content="{{route('case', ['url'=>$post->slug])}}" />
  @break
  @case('blog')
    <meta property="og:url" content="{{route('post', ['url'=>$post->slug])}}" />
  @break
@endswitch
<meta property="og:type" content="website"/>
<meta name="twitter:card" content="summary_large_image"/>
@if(isset($post->image))
  <meta property="og:image" content="{{ config('site.url') }}/{{ $post->image }}" />
@endif
