<ul class="navbar-nav mr-auto align-items-start align-items-md-center menu-main">
  <li class="nav-item"><a href="{{ route('tools') }}" class="nav-link" title="Tools">Tools</a></li>
  {{--<li class="nav-item"><a href="{{ route('blogs') }}" class="nav-link" title="Blog">Blog</a></li>--}}
  <li class="nav-item link-editor"><a href="{{ route('editor') }}" class="nav-link" title="Editor">Editor</a></li>
</ul>
@if(!Auth::check() || !Auth::user()->active)
  <ul class="navbar-nav ml-auto align-items-start align-items-md-center menu-login">
    <li class="nav-item"><a href="/login" class="btn btn-white-pink sign__link" id="header-login" data-tab="login" title="Login" onclick="if(typeof ym !== 'undefined') ym(48307115, 'reachGoal', 'loginclick'); return true;">Login</a></li>
    <li class="nav-item"><a href="/register" class="btn btn-white-pink sign__link" id="header-signup" data-tab="register" title="Sign up" onclick="if(typeof ym !== 'undefined') ym(48307115, 'reachGoal', 'signupclick'); return true;">Sign up</a></li>
  </ul>
@endif

@auth
  @if(Auth::user()->active)
    <ul class="navbar-nav ml-auto align-items-start align-items-md-center menu-login">
      @if(Auth::user()->hasRole(['admin', 'editor']))
        @if(isset($post))
          @if($post->type == 'blog')
            <li class="nav-item">
              <a class="nav-link" href="/admin/posts/{{$post->id}}?goback={{url()->current()}}">Edit</a>
            </li>
          @else
            <li class="nav-item">
              <a class="nav-link" href="/admin/{{$post->type}}s/{{$post->id}}?goback={{url()->current()}}">Edit</a>
            </li>
          @endif
        @endif
        <li class="nav-item">
          <a class="nav-link" href="/editor">Editor</a>
        </li>
        @if(Auth::user()->hasRole(['admin']))
          <li class="nav-item dropdown">
            <a id="dropdownSettings" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Settings</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownSettings">
              <a href="/admin/users" class="dropdown-item">Users</a>
              <a href="/admin/plans" class="dropdown-item">Plans</a>
              <a href="/admin/settings/tools" class="dropdown-item">Tools</a>
              <a href="/admin/projects/all" class="dropdown-item">Projects</a>
              <a href="/admin/payments/all" class="dropdown-item">Payments</a>
              <a href="/admin/fields" class="dropdown-item">Fields</a>
              <a href="/api/users/export" class="dropdown-item">Users export</a>
            </div>
          </li>
        @endif
        <li class="nav-item dropdown">
          <a id="dropdownContent" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Content</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownContent">
            <a href="/admin/pages" class="dropdown-item">Pages</a>
            <a href="/admin/posts" class="dropdown-item">Posts</a>
            <a href="/admin/tools" class="dropdown-item">Tools</a>
          </div>
        </li>
      @endif

      <li class="nav-item dropdown">
        <a id="navbarDropdown" class="user-nav-link nav-link dropdown-toggle d-flex align-items-center pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>{{ Auth::user()->name }}</span>
          <img class="user-nav-avatar" src="{{ $avatar }}" alt="">
          <span class="caret"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          @if(Auth::user()->hasRole(['admin', 'editor', 'user']))
            @if(Auth::user()->hasRole(['admin', 'editor']))
              <a href="/admin" class="dropdown-item">Dashboard</a>
            @endif
            <a href="/admin/projects" class="dropdown-item">Projects</a>
            <a href="/admin/subscriptions" class="dropdown-item">Subscriptions</a>
            <a href="/admin/payments" class="dropdown-item">Payments</a>
            <a href="/logout" class="dropdown-item">Sign out</a>
          @endif
        </div>
      </li>
    </ul>
  @endif
@endauth
