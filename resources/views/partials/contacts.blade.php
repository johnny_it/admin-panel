<div class="phones">
  <a href="tel:+7 (956) 213-21-78" rel="nofollow" class="phone" title="Позвонить"><span>+7 (956)</span> 213 21 78</a>
</div>
<div class="emails">
  <a href="mailto:info@paradox-std.ru" class="email" title="Написать">info@paradox-std.ru</a>
</div>
<div class="times">
  <span>С 9 до 20.00</span>
</div>
