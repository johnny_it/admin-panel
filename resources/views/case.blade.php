@extends('layouts.page')

@section('breadcrumbs')
  <div class="breadcrumbs">
    <div class="container">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/cases">Портфолио</a></li>
        <li class="breadcrumb-item"><a href="/cases/web">Web</a></li>
        <li class="breadcrumb-item active">Лендинг адвокатской конторы</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')
  <div class="case__top mt-3">
    <div class="container">
      <h1>лендинг для адвокатской <br>конторы</h1>
      <p>Юридическая помощь: консультация; оформление исковых заявлений в суды общей юрисдикции и арбитражные суды на территории РФ, адвокатских запросов в любые организации и правоохранительные органы; подготовка проектов любых видов сделок, в том числе юр.лицам по хозяйственной деятельности и составление иных правовых документов. И многое другое.</p>
    </div>
  </div>

  <div class="case__screen mt-5">
    <div class="container">
      <div class="case__screen_header"></div>
      <img src="/assets/images/cases/case-01-screen-home.jpg" alt="">
    </div>
  </div>

  <main class="content">
    <div class="container">

    </div>
  </main>

  <div class="case__relative mt-5">
    <div class="container">
      <div class="text-center"><a href="#" class="btn btn-transparent-pink case__relative_link">Похожие кейсы</a></div>

      <div class="cases cases-relative">
        <div class="tab-content header__cases_sliders mt-2 mt-lg-0 pt-lg-1" id="cases-tabContent">
          <div class="tab-pane fade show active" id="cases-web" role="tabpanel" aria-labelledby="cases-web-tab">
            <div class="row cases__items mt-4">
              <div class="col-sm-6 col-lg-4 my-3 cases__item">
                <a href="#" class="cases__link">
                  <div class="cases__img"><img src="/img/370x450/assets/images/cases/case-01.jpg" alt=""></div>
                  <div class="cases__title mt-2">Лендинг адвокатской конторе</div>
                </a>
              </div>
              <div class="col-sm-6 col-lg-4 my-3 cases__item">
                <a href="#" class="cases__link">
                  <div class="cases__img"><img src="/img/370x450/assets/images/cases/case-01.jpg" alt=""></div>
                  <div class="cases__title mt-2">Лендинг адвокатской конторе</div>
                </a>
              </div>
              <div class="col-sm-6 col-lg-4 my-3 cases__item">
                <a href="#" class="cases__link">
                  <div class="cases__img"><img src="/img/370x450/assets/images/cases/case-01.jpg" alt=""></div>
                  <div class="cases__title mt-2">Лендинг адвокатской конторе</div>
                </a>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="cases-branding" role="tabpanel" aria-labelledby="cases-branding-tab">
            <div class="row cases__items mt-4">
              <div class="col-sm-6 col-lg-4 my-3 cases__item">
                <a href="#" class="cases__link">
                  <div class="cases__img"><img src="/img/370x450/assets/images/cases/case-01.jpg" alt=""></div>
                  <div class="cases__title mt-2">Лендинг адвокатской конторе</div>
                </a>
              </div>
              <div class="col-sm-6 col-lg-4 my-3 cases__item">
                <a href="#" class="cases__link">
                  <div class="cases__img"><img src="/img/370x450/assets/images/cases/case-01.jpg" alt=""></div>
                  <div class="cases__title mt-2">Лендинг адвокатской конторе</div>
                </a>
              </div>
              <div class="col-sm-6 col-lg-4 my-3 cases__item">
                <a href="#" class="cases__link">
                  <div class="cases__img"><img src="/img/370x450/assets/images/cases/case-01.jpg" alt=""></div>
                  <div class="cases__title mt-2">Лендинг адвокатской конторе</div>
                </a>
              </div>
            </div>
          </div>
        </div>
        <ul class="nav nav-pills cases__tabs justify-content-center pl-xl-5 mt-4" id="cases-tab" role="tablist">
          <li class="nav-item mx-2 mx-xl-3">
            <a class="nav-link cases__tab active" id="cases-web-tab" data-toggle="pill" href="#cases-web" role="tab" aria-controls="cases-web" aria-selected="true">Web</a>
          </li>
          <li class="nav-item mx-2 mx-xl-3">
            <a class="nav-link cases__tab" id="cases-branding-tab" data-toggle="pill" href="#cases-branding" role="tab" aria-controls="cases-branding" aria-selected="false">Branding</a>
          </li>
        </ul>
      </div>
    </div>
  </div>

@endsection
