@extends('layouts.home')
@section('meta')
  @parent
  @if(isset($post->meta_title))
    <title>{{ $post->meta_title }}</title>
  @endif
  @if(isset($post->meta_keys))
    <meta name="keywords" content="{{ $post->meta_keys }}">
  @endif
  @if(isset($post->meta_desc))
    <meta name="description" content="{{ $post->meta_desc }}">
  @endif
@endsection

@section('content')
  <main class="content">
    <div class="container">
      @component('components.cases-list', ['cases'=>$cases, 'header'=>'Our cases'])
      @endcomponent
      @if($more)
        <div class="mt-4 text-center"><a class="btn btn-pink more-items" href="#" data-paginate-url="/api/pagination/posts/tool" data-offset="{{ $offset }}" data-display="{{ $display }}" data-items=".tools__items">Show more cases</a></div>
      @endif
    </div>
  </main>
@endsection
