@extends('layouts.page')

@section('content')
  <div class="container mt-4">
    <h2 class="text-center">Вход в админ панель</h2>
    <div class="row justify-content-center">
      <div class="col-md-8">
          <form class="ajaxauth mt-4" data-url="/api/login" method="POST" action="{{ route('login') }}">
              @csrf
              <div class="form-group row needs-validation" novalidate>
                  <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                  <div class="col-md-6">
                      <input type="email" class="form-control" value="admin@admin.ru" name="email" required autofocus>
                  </div>
              </div>

              <div class="form-group row">
                  <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                  <div class="col-md-6">
                      <input type="password" class="form-control" name="password" required value="12345">
                  </div>
              </div>

              <div class="form-group row mb-0">
                  <div class="col-md-5 offset-md-4">
                      <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>
@endsection

@section('bottom_scripts')
    <script>
      $('.ajaxauth').on('submit', function(e){
        e.preventDefault();
        var form = $(this);
        var url = $(this).data('url');

        $.ajax({
          url: url,
          data: $(this).serialize(),
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: 'POST',
          dataType: 'JSON',
        })
          .done(function (res) {
            if (res.success) {
              window.localStorage.removeItem('user');
              window.location.href = '/admin';
              //window.location.href = document.referrer;
            }
          })
          .fail(function(err){
            //remove errors
            form.find('input')
              .removeClass('is-invalid')
              .addClass('is-valid')
              .siblings('.invalid-feedback')
              .remove();

            var errors = JSON.parse(err.responseText).errors;
            Object.keys(errors).forEach(function (name) {
              form.find('input[name="'+name+'"]')
                .removeClass('is-valid')
                .addClass('is-invalid')
                .parent()
                .append('<div class="invalid-feedback">'+errors[name]+'</div>');
            });
          });
      });
    </script>
@endsection
