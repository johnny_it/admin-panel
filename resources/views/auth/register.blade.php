@extends('layouts.page')

@section('content')
  <div class="container mt-4">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <ul class="nav nav-pills slide__tabs justify-content-center" id="signtabs-page" role="tablist">
          <li class="nav-item">
            <a class="nav-link" id="tab-login-page" data-toggle="tab" href="#tab-content-login-page" role="tab"
               aria-controls="tab-content-login-page" aria-selected="true"
               onclick="ym(48307115, 'reachGoal', 'loginclick'); ga('send', 'event', 'ga_logincat', 'ga_loginact'); return true;">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" id="tab-register-page" data-toggle="tab" href="#tab-content-register-page" role="tab"
               aria-controls="tab-content-register-page" aria-selected="false"
               onclick="ym(48307115, 'reachGoal', 'signupclick'); ga('send', 'event', 'ga_signupcat', 'ga_signupact'); return true;">Sign
              up</a>
          </li>
        </ul>
        <div class="login-soc mt-4">
          <div class="row justify-content-center">
            @include('partials.login-social')
          </div>
        </div>
        <div class="row mt-4 justify-content-center">OR</div>

        <div class="tab-content mt-4" id="signtabs-page-content">
          <div class="tab-pane fade" id="tab-content-login-page" role="tabpanel"
               aria-labelledby="tab-login-page">
            @include('partials.login')
          </div>
          <div class="tab-pane fade show active" id="tab-content-register-page" role="tabpanel" aria-labelledby="tab-register-page">
            @include('partials.register')
          </div>
        </div>

      </div>
    </div>
  </div>
@endsection
