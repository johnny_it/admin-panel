@if(isset($posts) && count($posts))
  @if(isset($header))<h2 class="block-title">{{ $header }}</h2>@endif
<div class="row tools__items">
  @foreach ($posts as $article)
    <div class="col-lg-4 my-2 tools__item">
      <a href="{{ route('post', ['url' => $article['slug']]) }}" class="tools__link">
        <div class="tools__img"><img src="/img/640x360/{{ $article['image'] }}" alt="{{ $article['title'] }}"></div>
        <div class="tools__title mt-2">{{ $article['title'] }}</div>
      </a>
    </div>
  @endforeach
</div>
@endif
