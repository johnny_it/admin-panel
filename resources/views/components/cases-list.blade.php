@if(isset($cases) && count($cases))
  <h2 class="block-title">{{ $header }}</h2>
<div class="row cases__items">
  @foreach ($cases as $case)
    <div class="col-lg-4 my-2 cases__item">
      <a href="{{ route('case', ['url' => $case['slug']]) }}" class="cases__link">
        <div class="cases__img"><img src="/img/640x360/{{ $case['image'] }}" alt="{{ $case['title'] }}"></div>
        <div class="cases__title mt-2">{{ $case['title'] }}</div>
      </a>
    </div>
  @endforeach
</div>
@endif
