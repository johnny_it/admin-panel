<div class="alert alert-danger mb-4" role="alert">
  @foreach($errors as $error)
    <div class="my-2">{{ $error }}</div>
  @endforeach
</div>

<style>
  .alert {
    display: block;
  }
</style>
