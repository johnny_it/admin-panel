@if(isset($extentions))
  <div class="formats">
    <div class="container">
      <div class="formats__items">
        @foreach($extentions as $format)
          <div class="formats__item mt-4"><a class="formats__link @if($url == $preUrl.$format) active @endif" href="{{ route('tool.converter.video', [$preUrl.$format], false) }}" title="">{{ strtoupper($format) }}</a></div>
        @endforeach
      </div>
    </div>
  </div>
@endif
