@if(isset($plans) && count($plans))
  <div class="row plans__items">
    @if(isset($project))
      <div class="col-lg-4 mt-4 my-lg-0 plans__item plans-project">
        <div id="prices-plan1-card" class="card plans__card active" data-project="{{ $project->id }}" onclick="ym(48307115, 'reachGoal', 'plan1_click'); return true;">
          <div class="card-body">
            <h3 class="card-title plans__title">Create this video without a watermark</h3>
            <div class="plans__bottom mt-auto">
              <div class="plans__price mt-2">$ {{ config('price.single') }}</div>
              <div class="plans__period mt-2">one time payment</div>
            </div>
            @if(!Auth::check() || !Auth::user()->active)
              <a href="/register" class="btn btn-pink mt-4">Sign in</a>
            @endif
          </div>
        </div>
      </div>
      @else
        <div class="col-lg-4 mt-4 my-lg-0 plans__item plans-project">
          <div class="card plans__card inactive">
            <div class="card-body">
              <h3 class="card-title plans__title">Create this video without a watermark</h3>
              <div class="plans__bottom mt-auto">
                <div class="plans__price mt-2">$ {{ config('price.single') }}</div>
                <div class="plans__period mt-2">one time payment</div>
              </div>
              @if(!Auth::check() || !Auth::user()->active)
                <a href="/register" class="btn btn-pink mt-4">Sign in</a>
              @endif
            </div>
          </div>
        </div>
    @endif
    @foreach ($plans as $plan)
      <div class="col-lg-4 mt-4 my-lg-0 plans__item plans-month">
        <div id="prices-plan2-card" class="card plans__card @if(!isset($project)) active @endif" data-plan="{{ $plan['id'] }}" @if(isset($project)) data-plan-project="{{ $project->id }}" @endif onclick="ym(48307115, 'reachGoal', 'plan2_click'); return true;">
          <div class="card-body">
            <h3 class="card-title plans__title">{{ $plan['title'] }}</h3>
            <div class="plans__subtitle">for 1 month</div>
            <div class="plans__bottom mt-auto">
              <div class="plans__price mt-2">$ {{ $plan['price'] }}</div>
              <div class="plans__period mt-2">per month</div>
            </div>
            @if(!Auth::check() || !Auth::user()->active)
              <a href="/register" class="btn btn-pink mt-4">Sign in</a>
            @endif
          </div>
        </div>
      </div>
      <div class="col-lg-4 mt-4 my-lg-0 plans__item plans-year">
        <div id="prices-plan3-card" class="card plans__card" data-plan="{{ $plan['id'] }}" data-year="1" @if(isset($project)) data-plan-project="{{ $project->id }}" @endif onclick="ym(48307115, 'reachGoal', 'plan3_click'); return true;">
          <div class="card-body">
            <h3 class="card-title plans__title">{{ $plan['title'] }}</h3>
            <div class="plans__subtitle">for 1 year</div>
            <div class="plans__bottom mt-auto">
              <div class="plans__all">$ {{ $plan['price']*12 }}</div>
              <div class="row mt-2">
                <div class="col-xl-8 col-sm-7 col-6 plans__price">$ {{ $plan['price_year'] }}</div>
                <div class="col-xl-4 col-sm-5 col-6 plans-year__text">{!! $plan['discount_year_text'] !!}</div>
              </div>
              <div class="plans__period mt-2">per year</div>
            </div>
            @if(!Auth::check() || !Auth::user()->active)
              <a href="/register" class="btn btn-pink mt-4">Sign in</a>
            @endif
          </div>
        </div>
      </div>
    @endforeach
  </div>
@endif
