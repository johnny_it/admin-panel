@if(isset($plans) && count($plans))
  <h2 class="block-title">{{ $header }}</h2>
  <div class="row plans__items">
    @foreach ($plans as $plan)
      <div class="col-md-4 plans__item">
        <div class="card plans__card">
          <div class="card-body">
            <h5 class="card-title plans__title">{{ $plan['title'] }}</h5>
            <div class="card-text plans__desc">{!! $plan['description'] !!}</div>
            <div class="plans__choose form-group mt-auto">
              <div class="custom-control custom-radio">
              <input type="radio" id="price-month-{{ $plan['id'] }}" name="billing-{{ $plan['id'] }}" value="{{ $plan['price'] }}" data-period="month" class="custom-control-input" checked="checked">
              <label class="custom-control-label" for="price-month-{{ $plan['id'] }}">Monthly billing</label>
            </div>
              <div class="custom-control custom-radio">
                <input type="radio" id="price-year-{{ $plan['id'] }}" name="billing-{{ $plan['id'] }}" value="{{ $plan['price_year'] }}" data-period="year" class="custom-control-input">
                <label class="custom-control-label" for="price-year-{{ $plan['id'] }}">Yearly billing Save {{ $plan['discount_year'] }}%!</label>
              </div>
            </div>
            <p class="text-center plans__price"><span class="price">{{ $plan['price'] }}</span>$</p>
            <a href="/order" class="btn btn-primary plans__link" data-plan="{{ $plan['id'] }}">Buy</a>
          </div>
        </div>
      </div>
    @endforeach
  </div>
@endif
