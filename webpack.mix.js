const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/js/app.js', 'public/template/js')
  .js('resources/js/script.js', 'public/template/js')
  .sass('resources/sass/style.scss', 'public/template/css')
  .sass('resources/sass/app.scss', 'public/template/css')
  .options({
    processCssUrls: false
  })
  .browserSync({
    proxy: 'admin-panel',
    browser: 'chrome',
  });

if (mix.inProduction()) {
  mix.version();
}
