<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class RoleUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where('name', '=', 'admin')->first();
        $manager = Role::where('name', '=', 'manager')->first();
        $editor = Role::where('name', '=', 'editor')->first();
        $user = Role::where('name', '=', 'user')->first();

        $user1 = User::where('name', '=', 'Admin')->first();
        $user2 = User::where('name', '=', 'Manager')->first();
        $user3 = User::where('name', '=', 'Editor')->first();
        $user4 = User::where('name', '=', 'User')->first();

        $user1->attachRole($admin);
        $user2->attachRole($manager);
        $user3->attachRole($editor);
        $user4->attachRole($user);
    }
}
