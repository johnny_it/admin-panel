<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id' => 1,
            'name' => 'Admin',
            'email' => 'admin@admin.ru',
            'password' => Hash::make('12345'),
            //'image' => 'assets/users/1/images/admin.jpg',
            'active' => true,
        ]);
        User::create([
          'id' => 2,
          'name' => 'Manager',
          'email' => 'manager@manager.ru',
          'password' => Hash::make('12345'),
            //'image' => 'assets/users/2/images/editor.jpg',
          'active' => true,
        ]);
        User::create([
            'id' => 3,
            'name' => 'Editor',
            'email' => 'editor@editor.ru',
            'password' => Hash::make('12345'),
            //'image' => 'assets/users/2/images/editor.jpg',
            'active' => true,
        ]);
        User::create([
            'id' => 4,
            'name' => 'User',
            'email' => 'user@user.ru',
            'password' => Hash::make('12345'),
            //'image' => 'assets/users/3/images/user.jpg',
            'active' => true,
        ]);
    }
}
