<?php

use App\Template;
use Illuminate\Database\Seeder;

class TemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Template::create([
          'id' => 1,
          'name' => 'page',
          'title' => 'Страница',
        ]);
        Template::create([
          'id' => 2,
          'name' => 'blog',
          'title' => 'Блоговая страница',
        ]);
        Template::create([
          'id' => 3,
          'name' => 'portfolio',
          'title' => 'Портфолио',
        ]);
    }
}
