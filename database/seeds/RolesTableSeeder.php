<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
          'id' => 1,
          'name' => 'admin',
          'title' => 'Администратор',
        ]);
        Role::create([
          'id' =>2,
          'name' => 'manager',
          'title' => 'Менеджер',
        ]);
        Role::create([
          'id' => 3,
          'name' => 'editor',
          'title' => 'Редактор',
        ]);
        Role::create([
          'id' => 4,
          'name' => 'user',
          'title' => 'Пользователь',
        ]);
    }
}
