<?php

use App\Category;
use App\Post;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category1 = Category::create([
          'id' => 1,
          'title' => 'Категория 1',
          'description' => 'Описание категории 1',
        ]);

        $category2 = Category::create([
          'id' => 2,
          'parent_id' => $category1->id,
          'title' => 'Категория 2',
          'description' => 'Описание категории 2',
        ]);

        $category3 = Category::create([
          'id' => 3,
          'title' => 'Категория 3',
          'description' => 'Описание категории 3',
        ]);

        $post = Post::create([
          'user_id' => 1,
          'template_id' => 2,
          'type' => 'blog',
          'title' => 'Блоговая статья 1',
          'content' => '<p>Контент блоговой статьи.</p>',
        ]);
        $post->categories()->attach($category1->id, ['order'=>$category1->posts()->max('post_category.order')+1]);

        $post = Post::create([
          'user_id' => 1,
          'template_id' => 2,
          'type' => 'blog',
          'title' => 'Блоговая статья 2',
          'content' => '<p>Контент блоговой статьи.</p>',
        ]);
        $post->categories()->attach($category2->id, ['order'=>$category2->posts()->max('post_category.order')+1]);

        $post = Post::create([
          'user_id' => 1,
          'template_id' => 2,
          'type' => 'blog',
          'title' => 'Блоговая статья 3',
          'content' => '<p>Контент блоговой статьи.</p>',
        ]);
        $post->categories()->attach($category3->id, ['order'=>$category3->posts()->max('post_category.order')+1]);

        $post = Post::create([
          'user_id' => 1,
          'template_id' => 2,
          'type' => 'blog',
          'title' => 'Блоговая статья 4',
          'content' => '<p>Контент блоговой статьи.</p>',
        ]);
        $post->categories()->attach($category1->id, ['order'=>$category1->posts()->max('post_category.order')+1]);

        $post = Post::create([
          'user_id' => 1,
          'template_id' => 2,
          'type' => 'blog',
          'title' => 'Блоговая статья 5',
          'content' => '<p>Контент блоговой статьи.</p>',
        ]);
        $post->categories()->attach($category2->id, ['order'=>$category2->posts()->max('post_category.order')+1]);

        $post = Post::create([
          'user_id' => 1,
          'template_id' => 2,
          'type' => 'blog',
          'title' => 'Блоговая статья 6',
          'content' => '<p>Контент блоговой статьи.</p>',
        ]);
        $post->categories()->attach($category3->id, ['order'=>$category3->posts()->max('post_category.order')+1]);

        $post = Post::create([
          'user_id' => 1,
          'template_id' => 2,
          'type' => 'blog',
          'title' => 'Блоговая статья 7',
          'content' => '<p>Контент блоговой статьи.</p>',
        ]);
        $post->categories()->attach($category1->id, ['order'=>$category1->posts()->max('post_category.order')+1]);


    }
}
