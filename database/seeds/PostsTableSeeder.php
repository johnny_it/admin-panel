<?php

use App\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page1 = Post::create([
          'id' => 1,
          'user_id' => 1,
          'template_id' => 1,
          'type' => 'page',
          'title' => 'Главная страница',
          'url' => 'index',
          'description' => '',
          'content' => '<p>Контент главной страницы.</p>',
          'meta_title' => 'Paradox studio - студия верных решений',
        ]);
        $page2 = Post::create([
          'id' => 2,
          'user_id' => 1,
          'template_id' => 1,
          'type' => 'page',
          'title' => 'О нас',
          'url' => 'index',
          'description' => '',
          'content' => '<p>Контент страницы о нас.</p>',
          'meta_title' => 'Paradox studio - студия верных решений',
        ]);

        $child1 = Post::create([
          'id' => 3,
          'parent_id' => $page1->id,
          'user_id' => 1,
          'template_id' => 1,
          'type' => 'page',
          'title' => 'Внутренняя',
          'url' => 'index',
          'description' => '',
          'content' => '<p>Контент внутренней страницы.</p>',
          'meta_title' => 'Paradox studio - студия верных решений',
        ]);
        $child2 = Post::create([
          'id' => 4,
          'parent_id' => $page1->id,
          'user_id' => 1,
          'template_id' => 1,
          'type' => 'page',
          'title' => 'Внутренняя 2',
          'url' => 'index',
          'description' => '',
          'content' => '<p>Контент внутренней страницы.</p>',
          'meta_title' => 'Paradox studio - студия верных решений',
        ]);

        $child3 = Post::create([
          'id' => 5,
          'parent_id' => $page2->id,
          'user_id' => 1,
          'template_id' => 1,
          'type' => 'page',
          'title' => 'Внутренняя',
          'url' => 'index',
          'description' => '',
          'content' => '<p>Контент внутренней страницы.</p>',
          'meta_title' => 'Paradox studio - студия верных решений',
        ]);
        $child4 = Post::create([
          'id' => 6,
          'parent_id' => $page2->id,
          'user_id' => 1,
          'template_id' => 1,
          'type' => 'page',
          'title' => 'Внутренняя 2',
          'url' => 'index',
          'description' => '',
          'content' => '<p>Контент внутренней страницы.</p>',
          'meta_title' => 'Paradox studio - студия верных решений',
        ]);
    }
}
