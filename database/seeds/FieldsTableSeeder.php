<?php

use App\Field;
use App\Post;
use App\Template;
use Illuminate\Database\Seeder;

class FieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*$front = Field::create([
            'id' => 1,
            'type' => 'checkbox',
            'title' => 'Сделать главной страницей',
            'name' => 'front',
            'order' => Field::max('order') + 1
        ]);
        $firstPage = Post::find(1);
        $firstPage->fields()->attach([$front->id => ['value' => 1]]);
        $pageTemplate = Template::find(1);
        $pageTemplate->fields()->attach($front->id);*/
        $gallery = Field::create([
            'id' => 1,
            'type' => 'gallery',
            'title' => 'Галерея',
            'name' => 'gallery',
            'tab' => 1,
            'order' => Field::max('order') + 1
        ]);
        $inputText = Field::create([
            'id' => 2,
            'type' => 'text',
            'title' => 'Текстовое поле',
            'name' => 'test_text',
            'order' => Field::max('order') + 1
        ]);
        $inputText2 = Field::create([
            'id' => 3,
            'type' => 'text',
            'title' => 'Текстовое поле 2',
            'name' => 'test_text2',
            'order' => Field::max('order') + 1
        ]);
        $inputTextUnpublished = Field::create([
            'id' => 4,
            'type' => 'text',
            'title' => 'Неопубликованное текстовое поле',
            'name' => 'test_text_unpublish',
            'order' => Field::max('order') + 1,
            'published' => false,
        ]);
        $inputText3 = Field::create([
            'id' => 5,
            'type' => 'text',
            'title' => 'Текстовое поле 3',
            'name' => 'test_text3',
            'order' => Field::max('order') + 1
        ]);

        /*$firstPage = Post::find(1);
        $firstPage->fields()->attach([$front->id => ['value' => 1]]);*/
        $pageTemplate = Template::find(1);
        $pageTemplate2 = Template::find(2);
        $pageTemplate->fields()->attach($gallery->id, ['order'=>$pageTemplate->fields()->max('field_template.order')+1]);
        $pageTemplate->fields()->attach($inputText->id, ['order'=>$pageTemplate->fields()->max('field_template.order')+1]);

        $pageTemplate2->fields()->attach($gallery->id, ['order'=>$pageTemplate2->fields()->max('field_template.order')+1]);
        $pageTemplate2->fields()->attach($inputText->id, ['order'=>$pageTemplate2->fields()->max('field_template.order')+1]);

        $pageTemplate->fields()->attach($inputText2->id, ['order'=>$pageTemplate->fields()->max('field_template.order')+1]);
        $pageTemplate->fields()->attach($inputTextUnpublished->id, ['order'=>$pageTemplate->fields()->max('field_template.order')+1]);
        $pageTemplate->fields()->attach($inputText3->id, ['order'=>$pageTemplate->fields()->max('field_template.order')+1]);


        $pageTemplate2->fields()->attach($inputText3->id, ['order'=>$pageTemplate2->fields()->max('field_template.order')+1]);


        $firstPage = Post::find(1);
        $firstPage->fields()->attach($inputText->id, ['value'=>'Тестовое значение поля']);
        $firstPage->fields()->attach($inputText3->id, ['value'=>'Поле 3']);

    }
}
