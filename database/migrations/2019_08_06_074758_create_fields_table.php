<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('title');
            $table->string('name')->nullable();
            $table->text('options')->nullable();
            $table->text('default')->nullable();
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->text('extra')->nullable();
            $table->integer('order')->nullable();;
            $table->integer('tab')->default(0);
            $table->boolean('published')->default(1);
            $table->timestamps();
        });

        Schema::create('field_post', function (Blueprint $table) {
            $table->bigInteger('field_id')->unsigned();
            $table->bigInteger('post_id')->unsigned();
            $table->text('value')->nullable();

            $table->foreign('field_id')->references('id')->on('fields')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('post_id')->references('id')->on('posts')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['field_id', 'post_id']);
        });

        Schema::create('field_template', function (Blueprint $table) {
            $table->bigInteger('field_id')->unsigned();
            $table->bigInteger('template_id')->unsigned();
            $table->integer('order')->nullable();

            $table->foreign('field_id')->references('id')->on('fields')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('template_id')->references('id')->on('templates')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['field_id', 'template_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_post');
        Schema::dropIfExists('field_template');
        Schema::dropIfExists('fields');
    }
}
