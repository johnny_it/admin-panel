## Admin panel on Laravel 6.0 and Vue.js 2 and Element UI

#### Run next commands

1. update your .env file

2. composer install

3. php artisan migrate

4. php artisan db:seed

5. npm install or yarn

6. npm run dev

#### Admin panel

/admin - admin page 

/login - login page
