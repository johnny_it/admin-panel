<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Name of route
    |--------------------------------------------------------------------------
    |
    | Enter the routes name to enable dynamic imagecache manipulation.
    | This handle will define the first part of the URI:
    |
    | {route}/{template}/{filename}
    |
    | Examples: "images", "img/cache"
    |
    */

    'route' => 'img',

    /*
    |--------------------------------------------------------------------------
    | Storage paths
    |--------------------------------------------------------------------------
    |
    | The following paths will be searched for the image filename, submitted
    | by URI.
    |
    | Define as many directories as you like.
    |
    */

    'paths' => array(
        public_path('/'),
        public_path('upload'),
        public_path('images')
    ),

    /*
    |--------------------------------------------------------------------------
    | Manipulation templates
    |--------------------------------------------------------------------------
    |
    | Here you may specify your own manipulation filter templates.
    | The keys of this array will define which templates
    | are available in the URI:
    |
    | {route}/{template}/{filename}
    |
    | The values of this array will define which filter class
    | will be applied, by its fully qualified name.
    |
    */

    'templates' => array(
      'small' => 'Intervention\Image\Templates\Small',
      'medium' => 'Intervention\Image\Templates\Medium',
      'large' => 'Intervention\Image\Templates\Large',
      'avatar' => App\Filters\Avatar::class,
      '370x450' => App\Filters\Image370x450::class,
      '480x' => App\Filters\Image480x::class,
      '540x400' => App\Filters\Image540x400::class,
      '640x360' => App\Filters\Image640x360::class,
      '640x' => App\Filters\Image640x::class,
      'thumb' => App\Filters\Thumb128::class,
    ),

    /*
    |--------------------------------------------------------------------------
    | Image Cache Lifetime
    |--------------------------------------------------------------------------
    |
    | Lifetime in minutes of the images handled by the imagecache route.
    |
    */

    'lifetime' => 43200,

);
